﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance : MonoBehaviour
{

    public GameObject obj1, obj2;
    public double distance;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = Math.Abs(obj1.transform.position.z - obj2.transform.position.z);
    }
}
