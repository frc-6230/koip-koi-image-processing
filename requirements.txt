importlib-metadata==0.18
importlib-resources==3.0.0
numpy==1.16.1
opencv-python==4.0.0.21
Pillow==5.4.1
pynetworktables==2020.0.4
VideoCapture==0.9.5
wxPython==4.0.7.post2

