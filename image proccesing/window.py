import vidcap

import wx

from main import start_proc
from wxUtils import EditText, loadFile
from networktables import NetworkTables


class MyFrame(wx.Frame):

    def __init__(self):
        super().__init__(parent=None, title='KOIP')

        self.panel = wx.Panel(self)
        panel = self.panel

        f = panel.GetFont()
        dc = wx.WindowDC(panel)
        dc.SetFont(f)

        NetworkTables.initialize("127.0.0.1")

        padding = 5

        self.cameraID = EditText(panel, "Camera ID:", 5,5)

        self.camWidthET = EditText(panel,"Camera Width:", self.cameraID.x, self.cameraID.y + self.cameraID.getHeight() + padding)
        self.camHeightET = EditText(panel,"Camera Height:", self.camWidthET.x, self.camWidthET.y + self.camWidthET.getHeight() + padding)

        self.cameraFOVX = EditText(panel,"Camera FOV X:", self.camHeightET.x, self.camHeightET.y + self.camHeightET.getHeight() + padding)
        self.cameraFOVY = EditText(panel, "Camera FOV Y:", self.cameraFOVX.x,
                                   self.cameraFOVX.y + self.cameraFOVX.getHeight() + padding)

        self.select_file_btn = wx.Button(panel, wx.ID_OK, pos=(self.cameraFOVY.x,
                                                               self.cameraFOVY.y + self.cameraFOVY.getHeight() + padding), label='Choose Pipeline:')

        w, h = dc.GetTextExtent("No File Selected")
        self.pipeline = wx.StaticText(panel, label="No File Selected", pos=(self.select_file_btn.Rect[0] + self.select_file_btn.Rect[2] + padding, self.select_file_btn.Rect[1] +
                                                                      (self.select_file_btn.Rect[3] - h) / 2))

        self.startpross = wx.Button(panel, wx.ID_OK, pos=(self.cameraFOVX.x,
                                                          self.select_file_btn.Rect[1] + self.select_file_btn.Rect[3] + padding), label='Begin!')

        self.select_file_btn.Bind(wx.EVT_BUTTON, self.onFileSelectClick)

        self.startpross.Bind(wx.EVT_BUTTON, self.startProc)

        self.Show()

    def onFileSelectClick(self,event):
        file = loadFile(self.panel)

        if len(file) != 0:
            self.pipeline.SetLabelText(file)

    def startProc(self, event):

        id_ = width = height = fovx = fovy = None
        try:
            try:
                id_ = int(self.cameraID.text_ctrl.GetValue())
                width = int(self.camWidthET.text_ctrl.GetValue())
                height = int(self.camHeightET.text_ctrl.GetValue())
                fovx = float(self.cameraFOVX.text_ctrl.GetValue())
                fovy = float(self.cameraFOVY.text_ctrl.GetValue())

                if self.pipeline.GetLabel() == "No File Selected":
                    wx.MessageBox('Please select a pipeline', 'Error',
                                  wx.OK | wx.ICON_WARNING)
                    return

            except:
                wx.MessageBox('Invalid data entered', 'Error',
                              wx.OK | wx.ICON_WARNING)
                return

            self.startpross.SetLabelText("Stop!")
            start_proc(width, height, fovx, fovy, id_, self.pipeline.GetLabel())

        except AttributeError:
            wx.MessageBox('Make sure you generated GRIP pipeline with contours filtering at the end', 'Invalid Pipeline',
                          wx.OK | wx.ICON_WARNING)
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)

            wx.MessageBox(message,
                          'Invalid Webcam',
                          wx.OK | wx.ICON_WARNING)

        self.startpross.SetLabelText("Begin!")


if __name__ == '__main__':
    app = wx.App()
    frame = MyFrame()
    app.MainLoop()


