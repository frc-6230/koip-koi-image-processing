import sys
import time
from networktables import NetworkTables

name = sys.argv[1]
value = float(sys.argv[2])

NetworkTables.initialize("127.0.0.1")

table = NetworkTables.getTable('koip')

while True:
    table.putNumber(name, value)
    time.sleep(0.5)
    break
