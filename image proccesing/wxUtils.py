import wx


class EditText:

    def __init__(self, panel, textHint, x, y):

        f = panel.GetFont()
        dc = wx.WindowDC(panel)
        dc.SetFont(f)

        self.x = x
        self.y = y


        w, h = dc.GetTextExtent(textHint)

        self.text_ctrl = wx.TextCtrl(panel, pos=(x + w + 5, y))

        self.st = wx.StaticText(panel, label=textHint, pos=(x, y + (abs(self.text_ctrl.Rect[3] - h))/2))


    def getWidth(self):
        return max(self.text_ctrl.Rect[2], self.st.Rect[2])

    def getHeight(self):
        return max(self.text_ctrl.Rect[3], self.st.Rect[3])



def loadFile(panel):
    openFileDialog = wx.FileDialog(panel, "Select pipeline", "", "",
                                   "Python files (*.py)|*.py",
                                   wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
    openFileDialog.ShowModal()

    return openFileDialog.GetPath()

#----------------------------------------------------------------------
def saveFile(panel):
    saveFileDialog = wx.FileDialog(panel, "Save As", "", "",
                                   "Python files (*.py)|*.py",
                                   wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
    saveFileDialog.ShowModal()
    return saveFileDialog.GetPath()


