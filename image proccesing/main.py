import math
import sys
import cv2
import numpy as np
from VideoCapture import Device
import importlib.util
from networktables import NetworkTables


# start = False

def start_proc(width, height, fovX, fovY, cameraID, pipePath):
    # global start
    # start = not start
    #
    # if not start:
    #     return
    NetworkTables.initialize("127.0.0.1")

    table = NetworkTables.getTable('koip')

    spec = importlib.util.spec_from_file_location("pipeline",
                                                  pipePath)
    pipe = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(pipe)

    pipeline = pipe.Pipeline()

    focalX = width / (2 * math.tan(math.radians(fovX / 2)))
    focalY = height / (2 * math.tan(math.radians(fovY / 2)))

    cam = Device(cameraID)
    cam.setResolution(width, height)

    while (True):

        img = np.array(cam.getImage())
        img = cv2.cvtColor(img, cv2.COLOR_RGBA2BGR)

        out = img.copy()

        pipeline.process(out)

        if len(pipeline.filter_contours_output) > 0:

            cont = pipeline.filter_contours_output[0]

            M = cv2.moments(cont)

            x, y, w, h = cv2.boundingRect(cont)

            approx = cv2.approxPolyDP(cont, 0.01 * cv2.arcLength(cont, True), True)


            if M["m00"] != 0:
                cv2.drawContours(out, pipeline.find_contours_output, 0, (128, 0, 128), 3)
                #
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                # draw the contour and center of the shape on the image
                cv2.circle(out, (cX, cY), 7, (255, 255, 255), -1)
                cv2.putText(out, "center", (cX - 20, cY - 20),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

                tx = math.degrees(math.atan2(cX - width / 2, focalX))

                cv2.putText(out, "tx: " + str(tx), (20, 20),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

                ty = math.degrees(math.atan2(cY - height / 2, focalY))

                cv2.putText(out, "ty: " + str(ty), (20, 40),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

                area = cv2.contourArea(cont) / (width * height)

                cv2.putText(out, "area: " + str(area), (20, 60),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

                point_count = 0
                point_x_array = []
                point_y_array = []

                for arr in approx:
                    for arr2 in arr:
                        cv2.circle(out, (arr2[0], arr2[1]), 7, (0,0,255), -1)
                        cv2.putText(out, str(point_count), (arr2[0] + 6, arr2[1]),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,255), 2)
                        point_count += 1
                        point_x_array.append(arr2[0])
                        point_y_array.append(arr2[1])

                table.putNumber('tx', tx)
                table.putNumber('ty', ty)
                table.putNumber('ta', area)
                table.putBoolean('tv', True)
                table.putNumberArray("tcornx", point_x_array)
                table.putNumberArray("tcorny", point_y_array)
        else:
            table.putBoolean('tv', False)



        cv2.imshow('frame', img)
        cv2.imshow('out', out)

        cv2.imshow("tresh", pipeline.hsv_threshold_output)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cv2.destroyAllWindows()


width = int(sys.argv[1])
height = int(sys.argv[2])
fovX = float(sys.argv[3])
fovY = float(sys.argv[4])
cameraID = int(sys.argv[5])
pipe = sys.argv[6]

start_proc(width,height,fovX,fovY,cameraID, pipe)